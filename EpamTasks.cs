using System;
using System.Text;

namespace EpamTasks
{
	struct Vector
	{
		private double _x;
		private double _y;
		private double _z;
		
		public Vector(double x, double y, double z)
		{
			_x = x;
			_y = y;
			_z = z;
		}
		
		public double Length
		{
			get
			{
				return Math.Sqrt(Math.Abs(_x * _x + _y * _y + _z * _z));
			}
		}
		
		public static Vector operator +(Vector vector1, Vector vector2)
		{
			return new Vector(vector1._x + vector2._x, vector1._y + vector2._y, vector1._z + vector2._z);
		}
		
		public static Vector operator -(Vector vector1, Vector vector2)
		{
			return new Vector(vector1._x - vector2._x, vector1._y - vector2._y, vector1._z - vector2._z);
		}
		
		public static double operator *(Vector vector1, Vector vector2)
		{
			return vector1._x * vector2._x + vector1._y * vector2._y + vector1._z * vector2._z;
		}
		
		public static Vector CrossProduct(Vector vector1, Vector vector2)
		{
			return new Vector(vector1._y * vector2._z - vector1._z * vector2._y, vector1._x * vector2._z - vector1._z * vector2._x, vector1._y * vector2._x - vector1._x * vector2._y);
		}
		
		public static double TripleProduct(Vector vector1, Vector vector2, Vector vector3)
		{
			return vector1 * Vector.CrossProduct(vector2, vector3);
		}
		
		public static double AngleBetween(Vector vector1, Vector vector2)
		{
			return Math.Acos((vector1 * vector2) / (vector1.Length * vector2.Length));
		}
		
		public override bool Equals(object obj)
		{
			// If parameter is null return false to avoid unnecessary step with creating new vector and "as"
			if (obj == null)
			{
				return false;
			}

			Vector other = (Vector)obj;

			if (other == null)
			{
				return false;
			}

			return this._x == other._x && this._y == other._y && this._z == other._z;
		}
		
		// Equals for own types to improve performance
		public bool Equals(Vector other)
		{

			if ((object)other == null)
			{
				return false;
			}

			return this._x == other._x && this._y == other._y && this._z == other._z;
		}
		
		public static bool Equals( Vector vector1, Vector vector2)
		{
			// If both are null, or both are same instance, return true
			if (Object.ReferenceEquals(vector1, vector2))
			{
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)vector1 == null) || ((object)vector2 == null))
			{
				return false;
			}

			// Return true if the fields match.
			return vector1._x == vector2._x && vector1._y == vector2._y && vector1._z == vector2._z;
		}
		
		public override int GetHashCode()
		{
			return (int)_x ^ (int)_y ^ (int)_z;
		}
		
		public static bool operator ==(Vector vector1, Vector vector2)
		{
			return Equals(vector1, vector2);
		}

		public static bool operator !=(Vector vector1, Vector vector2)
		{
			return !Equals(vector1, vector2);
		}
		
		public static bool operator >(Vector vector1, Vector vector2)
		{
			return vector1.Length > vector2.Length;
		}
		
		public static bool operator <(Vector vector1, Vector vector2)
		{
			return vector1.Length < vector2.Length;
		}
		
		public static Vector Max(Vector vector1, Vector vector2)
		{
			return (vector1 > vector2 ? vector1 : vector2).Copy();
		}
		
		public static Vector Min(Vector vector1, Vector vector2)
		{
			return (vector1 < vector2 ? vector1 : vector2).Copy();
		}
		
		public Vector Copy()
		{
			return new Vector(_x, _y, _z);
		}
		
		public override string ToString()
		{
			return String.Format("({0}; {1}; {2})", _x, _y, _z);
		}
	}
	
	class Rectangle
	{
		private double _x;
		private double _y;
		private double _width;
		private double _height;
		
		public Rectangle()
		{
			_x = 0.0;
			_y = 0.0;
			_width = 0.0;
			_height = 0.0;
		}
		
		public Rectangle(double x, double y, double width, double height)
		{
			_x = x;
			_y = y;
			_width = width;
			_height = height;
		}
		
		public void ChangeLocation(double x, double y)
		{
			_x = x;
			_y = y;
		}
		
		public void ChangeSize(double width, double height)
		{
			_width = width;
			_height = height;
		}
		
		public void ChangeWidth(double width)
		{
			_width = width;
		}
		
		public void ChangeHeight(double height)
		{
			_height = height;
		}
		
		public static Rectangle Union(Rectangle rectangle1, Rectangle rectangle2)
		{
			double x1 = Math.Min(rectangle1._x, rectangle2._x);
            double x2 = Math.Max(rectangle1._x + rectangle1._width, rectangle2._x + rectangle2._width); 
            double y1 = Math.Max(rectangle1._y, rectangle2._y);
            double y2 = Math.Min(rectangle1._y - rectangle1._height, rectangle2._y - rectangle2._height);
 
            return new Rectangle(x1, y1, x2 - x1, y1 - y2); 
		}
		
		public static Rectangle Intersect(Rectangle rectangle1, Rectangle rectangle2) 
		{
            double x1 = Math.Max(rectangle1._x, rectangle2._x);
            double x2 = Math.Min(rectangle1._x + rectangle1._width, rectangle2._x + rectangle2._width); 
            double y1 = Math.Min(rectangle1._y, rectangle2._y);
            double y2 = Math.Max(rectangle1._y - rectangle1._height, rectangle2._y - rectangle2._height); 
  
            if (x2 >= x1 && y1 >= y2) 
			{ 
 
                return new Rectangle(x1, y1, x2 - x1, y1 - y2);
            }
            return new Rectangle(); 
        }
		
		public override string ToString()
		{
			return String.Format("({0}; {1}) width: {2}, height: {3}", _x, _y, _width, _height);
		}
	}
	
	class LinealArray
	{
		Array _data;
		private int _startIndex;
		private int _endIndex;
		
		public LinealArray(int startIndex, int endIndex, int[] arr)
		{
			if (endIndex - startIndex + 1 != arr.Length)
			{
				Console.WriteLine("Error");
				return;
			}
			
			_startIndex = startIndex;
			_endIndex = endIndex;
			
			_data = Array.CreateInstance(typeof(int), new int[1] { endIndex }, new int[1] { startIndex });
			
			int j = 0;
			for (int i = startIndex; i <= endIndex; i++)
			{
				_data.SetValue(arr[j++], i);
			}
		}
		
		public int Length
		{
			get { return _endIndex - _startIndex + 1; }
		}
		
		public override string ToString()
		{
			// Use StringBuilder when there are many concatenation to improve performance
			StringBuilder s = new StringBuilder();
			
			for (int i = _startIndex; i <= _endIndex; i++)
			{
				s.Append(String.Format("{0} ", _data.GetValue(i)));
			}
			return s.ToString();
		}
		
		public int this [int index]
		{

            set 
			{ 
				if (index > _endIndex || index < _startIndex)
				{
					throw new IndexOutOfRangeException();
				}
				_data.SetValue(value, index); 
			}
            get 
			{ 
				return (int)_data.GetValue(index); 
			}

        }
		
		public static LinealArray Addition(LinealArray array1, LinealArray array2)
		{
			if (!CheckRange(array1, array2))
			{
				throw new Exception("There are different index ranges in arrays.");
			}
			
			int[] arr = new int[array1.Length];
			int start = array1._startIndex;
			int end = array1._endIndex;
			int j = 0;
			
			for (int i = start; i <= end; i++)
			{
				arr[j++] = array1[i] + array2[i];
			}
			
			return new LinealArray(start, end, arr);
		}
		
		public static LinealArray Subtraction(LinealArray array1, LinealArray array2)
		{
			if (!CheckRange(array1, array2))
			{
				throw new Exception("There are different index ranges in arrays.");
			}
			
			int[] arr = new int[array1.Length];
			int start = array1._startIndex;
			int end = array1._endIndex;
			int j = 0;
			
			for (int i = start; i <= end; i++)
			{
				arr[j++] = array1[i] - array2[i];
			}
			
			return new LinealArray(start, end, arr);
		}
		
		public static LinealArray operator *(LinealArray array, int number)
		{
			int[] tmpArray = new int[array.Length];
			int j = 0;
			
			for (int i = array._startIndex; i <= array._endIndex; i++)
			{
				tmpArray[j++] = array[i] * number;
			}
			
			return new LinealArray(array._startIndex, array._endIndex, tmpArray);
		}
		
		public void Multiply(int number)
		{			
			for (int i = this._startIndex; i <= this._endIndex; i++)
			{
				this[i] = this[i] * number;
			}
		}
		
		public override bool Equals(object obj)
		{
			// If parameter is null return false to avoid unnecessary step with creating new array
			if (obj == null)
			{
				return false;
			}
			
			LinealArray other = (LinealArray)obj;

			if (other == null)
			{
				return false;
			}
			
			if (!CheckRange(this, other))
			{
				return false;
			}
			
			for (int i = this._startIndex; i <= this._endIndex; i++)
			{
				if (this[i] != other[i])
				{
					return false;
				}
			}
			
			return true;
		}
		
		// Equals for own types to improve performance
		public bool Equals(LinealArray other)
		{

			if ((object)other == null)
			{
				return false;
			}
			
			if (!CheckRange(this, other))
			{
				return false;
			}
			
			for (int i = this._startIndex; i <= this._endIndex; i++)
			{
				if (this[i] != other[i])
				{
					return false;
				}
			}
			
			return true;
		}
		
		public static bool Equals( LinealArray array1, LinealArray array2)
		{
			// If both are null, or both are same instance, return true
			if (Object.ReferenceEquals(array1, array2))
			{
				return true;
			}

			// If one is null, but not both, return false
			if (((object)array1 == null) || ((object)array2 == null))
			{
				return false;
			}
			
			if (!CheckRange(array1, array2))
			{
				return false;
			}
			
			for (int i = array1._startIndex; i <= array2._endIndex; i++)
			{
				if (array1[i] != array2[i])
				{
					return false;
				}
			}
			
			return true;
		}
		
		public override int GetHashCode()
		{
			return (int)_data.GetValue(_startIndex) ^ (int)_data.GetValue(_endIndex);
		}
		
		public static bool operator ==(LinealArray array1, LinealArray array2)
		{
			return Equals(array1, array2);
		}

		public static bool operator !=(LinealArray array1, LinealArray array2)
		{
			return !Equals(array1, array2);
		}
		
		private static bool CheckRange(LinealArray array1, LinealArray array2)
		{
			if (array1._startIndex != array2._startIndex || array1._endIndex != array2._endIndex)
			{
				return false;
			}
			return true;
		}
	}
	
	public class Matrix
	{
		private double[,] _innerMatrix;
		private int _rowCount = 0;
		private int _columnCount = 0;

		public Matrix(int rowCount, int columnCount)
		{
			_rowCount = rowCount;
			_columnCount = columnCount;
			_innerMatrix = new double[rowCount, columnCount];
		}
		
		public Matrix(int rowCount, int columnCount, double[,] matrix)
		{
			if (rowCount != matrix.GetLength(0) || columnCount != matrix.GetLength(1))
			{
				throw new Exception("Wrong matrix");
			}
			
			_rowCount = rowCount;
			_columnCount = columnCount;
			_innerMatrix = new double[rowCount, columnCount];
			
			for (int i = 0; i < RowCount; i++)
			{
				for (int j = 0; j < ColumnCount; j++)
				{
					_innerMatrix[i, j] = matrix[i, j];
				}
			}
		}
		
		public int RowCount
		{
			get { return _rowCount; }
		}
		
		public int ColumnCount
		{
			get { return _columnCount; }
		}
		
		public double this[int rowNumber,int columnNumber]
		{
			get
			{
				return _innerMatrix[rowNumber, columnNumber];
			}
			set
			{
				_innerMatrix[rowNumber, columnNumber] = value;
			}
		}

		public double[] GetRow(int rowIndex)
		{
			double[] rowValues = new double[_columnCount];
			
			for (int i = 0; i < _columnCount; i++)
			{
				rowValues[i] = _innerMatrix[rowIndex, i];
			}
			
			return rowValues;
		}
		
		public void SetRow(int rowIndex, double[] array)
		{
			if (array.Length != _columnCount)
			{
				throw new Exception("Wrong length of input array");
			}
			
			for (int i = 0; i < array.Length; i++)
			{
				_innerMatrix[rowIndex, i] = array[i];
			}
		}
		
		public double[] GetColumn(int columnIndex)
		{ 
			double[] columnValues = new double[_rowCount];
			
			for (int i = 0; i < _rowCount; i++)
			{
				columnValues[i] = _innerMatrix[i, columnIndex];
			}
			
			return columnValues;
		}
		
		public void SetColumn(int columnIndex,double[] array)
		{ 
			if (array.Length != _rowCount)
			{
				throw new Exception("Wrong length of input array");
			}
			
			for (int i = 0; i < array.Length; i++)
			{
				_innerMatrix[i, columnIndex] = array[i];
			}
		}

		public static Matrix operator +(Matrix matrix1, Matrix matrix2)
		{
			if (!(matrix1.RowCount == matrix2.RowCount && matrix1.ColumnCount == matrix2.ColumnCount))
			{
				throw new Exception("Different sizes of matrixes");
			}
			
			Matrix returnMartix = new Matrix(matrix1.RowCount, matrix1.ColumnCount);
			
			for (int i = 0; i < matrix1.RowCount; i++)
			{
				for (int j = 0; j < matrix1.ColumnCount; j++)
				{
					returnMartix[i, j] = matrix1[i, j] + matrix2[i, j];
				}
			}
			return returnMartix;
		}
		
		public static Matrix operator *(Matrix matrix, double scalarValue)
		{
			Matrix returnMartix = new Matrix(matrix.RowCount, matrix.ColumnCount);
			
			for (int i = 0; i < matrix.RowCount; i++)
			{
				for (int j = 0; j < matrix.ColumnCount; j++)
				{
					returnMartix[i, j] = matrix[i, j] * scalarValue;
				}
			}
			
			return returnMartix;
		}
		
		public static Matrix operator -(Matrix matrix1, Matrix matrix2)
		{
			if (!(matrix1.RowCount == matrix2.RowCount && matrix1.ColumnCount == matrix2.ColumnCount))
			{
				throw new Exception("Different sizes of matrixes");
			}
			return matrix1 + (matrix2 * (-1));
		}
		
		public static bool operator ==(Matrix matrix1, Matrix matrix2)
		{
			return Equals(matrix1, matrix2);
		}
		
		public static bool operator !=(Matrix matrix1, Matrix matrix2)
		{
			return !(matrix1 == matrix2);
		}
		
		public static Matrix operator -(Matrix matrix)
		{
			return matrix * (-1);
		}
		
		public static Matrix operator ++(Matrix matrix)
		{
			for (int i = 0; i < matrix.RowCount; i++)
			{
				for (int j = 0; j < matrix.ColumnCount; j++)
				{
					matrix[i, j] += 1;
				}
			}
			return matrix;
		}
		
		public static Matrix operator --(Matrix matrix)
		{
			for (int i = 0; i < matrix.RowCount; i++)
			{
				for (int j = 0; j < matrix.ColumnCount; j++)
				{
					matrix[i, j] -= 1;
				}
			}
			return matrix;
		}
		
		public static Matrix operator *(Matrix matrix1, Matrix matrix2)
		{
			if (matrix1.ColumnCount != matrix2.RowCount)
			{
				throw new Exception("Wrong sizes of matrixes");
			}
			
			Matrix returnMatrix = new Matrix(matrix1.RowCount, matrix2.ColumnCount);
			
			for (int i = 0; i < matrix1.RowCount; i++)
			{
				double[] rowValues = matrix1.GetRow(i);
				
				for (int j = 0; j < matrix2.ColumnCount; j++)
				{
					double[] columnValues = matrix2.GetColumn(j);
					double value = 0;
					
					for (int a = 0; a < rowValues.Length; a++)
					{
						value += rowValues[a] * columnValues[a];
					}
					
					returnMatrix[i, j] = value;
				}
			}
			
			return returnMatrix;
		}
		
		public Matrix Transpose()
		{
			Matrix returnMartix = new Matrix(ColumnCount, RowCount);
			
			for (int i = 0; i < RowCount; i++)
			{
				for (int j = 0; j < ColumnCount; j++)
				{
					returnMartix[j, i] = _innerMatrix[i, j];
				}
			}
			
			return returnMartix;
		}
		
		//-------------------------------------------------------------
		public override bool Equals(object obj)
		{
			// If parameter is null return false to avoid unnecessary step with creating new matrix
			if (obj == null)
			{
				return false;
			}
			
			Matrix other = (Matrix)obj;

			if (other == null)
			{
				return false;
			}
			
			if (!CheckRange(this, other))
			{
				return false;
			}
			
			for (int i = 0; i < this.RowCount; i++)
			{
				for (int j = 0; j < this.ColumnCount; j++)
				{
					if (this[i, j] != other[i, j])
					{
						return false;
					}
				}
			}
			
			return true;
		}
		
		// Equals for own types to improve performance
		public bool Equals(Matrix other)
		{

			if ((object)other == null)
			{
				return false;
			}
			
			if (!CheckRange(this, other))
			{
				return false;
			}
			
			for (int i = 0; i < this.RowCount; i++)
			{
				for (int j = 0; j < this.ColumnCount; j++)
				{
					if (this[i, j] != other[i, j])
					{
						return false;
					}
				}
			}
			
			return true;
		}
		
		public static bool Equals(Matrix matrix1, Matrix matrix2)
		{
			// If both are null, or both are same instance, return true
			if (Object.ReferenceEquals(matrix1, matrix2))
			{
				return true;
			}

			// If one is null, but not both, return false
			if (((object)matrix1 == null) || ((object)matrix2 == null))
			{
				return false;
			}
			
			if (!CheckRange(matrix1, matrix2))
			{
				return false;
			}
			
			for (int i = 0; i < matrix1.RowCount; i++)
			{
				for (int j = 0; j < matrix1.ColumnCount; j++)
				{
					if (matrix1[i, j] != matrix2[i, j])
					{
						return false;
					}
				}
			}
			
			return true;
		}
		//-------------------------------------------------------------
		
		
		public override int GetHashCode()
		{
			return (int)_innerMatrix[0, 0] ^ (int)_innerMatrix[_rowCount, _columnCount];
		}

		public bool IsZeroMatrix()
		{
			for (int i = 0; i < this.RowCount; i++)
			{
				for (int j = 0; j < this.ColumnCount; j++)
				{
					if (_innerMatrix[i, j] != 0)
					{
						return false;
					}
				}
			}
			
			return true;
		}
		
		public bool IsSquareMatrix()
		{
			return (this.RowCount == this.ColumnCount);
		}
		
		public bool IsLowerTriangle()
		{
			if (!this.IsSquareMatrix())
			{
				return false;
			}
			
			for (int i = 0; i < this.RowCount; i++)
			{
				for (int j = i + 1; j < this.ColumnCount; j++)
				{
					if (_innerMatrix[i, j] != 0)
					{
						return false;
					}
				}
			}
			
			return true;
		}
		
		public bool IsUpperTriangle()
		{
			if (!this.IsSquareMatrix())
			{
				return false;
			}
			
			for (int i = 0; i < this.RowCount; i++)
			{
				for (int j = 0; j < i; j++)
				{
					if (_innerMatrix[i, j] != 0)
					{
						return false;
					}
				}
			}
			
			return true;
		}
		
		public bool IsDiagonalMatrix()
		{
			if (!this.IsSquareMatrix())
			{
				return false;
			}
			
			for (int i = 0; i < this.RowCount; i++)
			{
				for (int j = 0; j < this.ColumnCount; j++)
				{
					if (i != j && _innerMatrix[i, j] != 0)
					{
						return false;
					}
				}
			}
			
			return true;
		}
		
		public bool IsIdentityMatrix()
		{
			if (!this.IsSquareMatrix())
			{
				return false;
			}
			
			for (int i = 0; i < this.RowCount; i++)
			{
				for (int j = 0; j < this.ColumnCount; j++)
				{
					double checkValue = 0;
					if (i == j)
					{
						checkValue = 1;
					}
					
					if (_innerMatrix[i, j] != checkValue)
					{
						return false;
					}
				}
			}
			
			return true;
		}

		public bool IsSymetricMatrix()
		{
			if (!this.IsSquareMatrix())
			{
				return false;
			}
			
			Matrix transposeMatrix = this.Transpose();
			
			if (this == transposeMatrix)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public Matrix SubMatrix(int startRowIndex, int startColumnIndex, int rowCount, int columnCount)
		{
			if (rowCount > RowCount - startRowIndex || columnCount > ColumnCount - startColumnIndex || rowCount < 1 || columnCount < 1)
			{
				throw new Exception("The number of rows or columns is too large or less than one!");
			}				
			if (startRowIndex < 0 || startColumnIndex < 0 || startRowIndex > RowCount || startColumnIndex > ColumnCount)
			{
				throw new IndexOutOfRangeException("Index row or column is too large or less than zero!");
			}
			
			Matrix returnMatrix = new Matrix(rowCount, columnCount);
			int i = 0, j = 0;
			
			for (int a = startRowIndex; a <= rowCount; a++)
			{
				j = 0;
				for (int b = startColumnIndex; b <= columnCount; b++)
				{
					returnMatrix[i, j] = this[a, b];
					j++;
				}
				i++;
			}
			
			return returnMatrix;
		}
		
		public override string ToString()
		{
			// Use StringBuilder when there are many concatenation to improve performance
			StringBuilder s = new StringBuilder();
			
			s.Append(String.Format("({0}x{1})\n", this.RowCount, this.ColumnCount));
			
			for (int i = 0; i < RowCount; i++)
			{
				for (int j = 0; j < ColumnCount; j++)
				{
					s.Append(String.Format("{0}\t", this[i, j]));
				}
				s.Append("\n");
			}
	
			return s.ToString();
		}
		
		private static bool CheckRange(Matrix matrix1, Matrix matrix2)
		{
			if (matrix1.RowCount != matrix2.RowCount || matrix1.ColumnCount != matrix2.ColumnCount)
			{
				return false;
			}
			return true;
		}
	}
	
	class EntryPoint
	{
		static void Main()
		{
			// First task
			var vec1 = new Vector(1.3, 2.0, 3.2);
			var vec2 = new Vector(4.2, 5.1, 6.7);
			var vec3 = new Vector(1.3, 2.0, 3.2);
			Console.WriteLine("\n******************************FIRST TASK*****************************\n");
			Console.WriteLine("First vector: {0}", vec1);
			Console.WriteLine("Second vector: {0}", vec2);
			Console.WriteLine("Third vector: {0}\n", vec3);
			
			Console.WriteLine("First + second = {0}", vec1 + vec2);
			Console.WriteLine("First - second = {0}", vec1 - vec2);
			Console.WriteLine("First * second = {0}", vec1 * vec2);
			Console.WriteLine("Cross product of first and second = {0}", Vector.CrossProduct(vec1, vec2));
			Console.WriteLine("Triple product of first, second and third = {0}", Vector.TripleProduct(vec1, vec2, vec3));
			Console.WriteLine("Length of first = {0}", vec1.Length);
			Console.WriteLine("Angle between first and second = {0}", Vector.AngleBetween(vec1, vec2));
			Console.WriteLine("Bigger between the first and second = {0}", Vector.Max(vec1, vec2));
			
			if (vec1 == vec3)
			{
				Console.WriteLine("First and third are equals");
			}
			if (vec1 != vec2)
			{
				Console.WriteLine("First and second are not equals");
			}
			if (vec1 > vec2)
			{
				Console.WriteLine("First is larger than second");
			}
			if (vec1 < vec2)
			{
				Console.WriteLine("Second is larger than first");
			}			
			
			// Second task
			Rectangle rec1 = new Rectangle(4, 2, 3, 2);
			Rectangle rec2 = new Rectangle(3, 5, 1, 2);
			Console.WriteLine("\n******************************SECOND TASK****************************\n");
			Console.WriteLine("First rectangle: {0}", rec1);
			Console.WriteLine("Second rectangle: {0}\n", rec2);
			
			rec1.ChangeLocation(1, 3);
			Console.WriteLine("First rectangle with changed location: {0}", rec1);
			rec2.ChangeSize(2, 3);
			Console.WriteLine("Second rectangle with changed size: {0}", rec2);
			Console.WriteLine("Union of both rectangles: {0}", Rectangle.Union(rec1, rec2));
			Console.WriteLine("Intersection of both rectangles: {0}", Rectangle.Intersect(rec1, rec2));
			
			// Third task
			int[] tmp1 = {2, 6, 3, 4, 7, 9, 1, 8};
			int[] tmp2 = {3, 5, 1, 2, 4, 7, 5, 6};
			LinealArray arr1 = new LinealArray(3, 10, tmp1);
			LinealArray arr2 = new LinealArray(3, 10, tmp2);
			LinealArray arr3 = new LinealArray(3, 10, tmp2);
			Console.WriteLine("\n******************************THIRD TASK****************************\n");
			Console.WriteLine("First array: {0}", arr1);
			Console.WriteLine("Second array: {0}\n", arr2);
			Console.WriteLine("Element by index 4 in first array: {0}", arr1[4]);
			Console.WriteLine("First array plus second array: {0}", LinealArray.Addition(arr1, arr2));
			Console.WriteLine("First array minus second array: {0}", LinealArray.Subtraction(arr1, arr2));
			Console.WriteLine("First array multiplied by 2: {0}", arr1 * 2);
			
			if (arr2 == arr3)
			{
				Console.WriteLine("Second and third are equals");
			}
			if (arr1 != arr2)
			{
				Console.WriteLine("First and second are not equals");
			}
			
			// Fourth task
			double[,] tmp3 = {{5, 1, 4, 3, 2}, {6, 2, 4, 5, 1}, {4, 2, 8, 9, 5}, {3, 0, 2, 7, 4}};
			double[,] tmp4 = {{2, 3, 1, 0, 5}, {7, 3, 5, 4, 3}, {3, 1, 7, 4, 9}, {4, 6, 1, 8, 7}};
			double[,] tmp5 = {{1, 4, 2, 4, 8}, {6, 2, 4, 1, 8}, {7, 6, 9, 2, 4}, {4, 5, 7, 2, 1}, {5, 0, 6, 2, 9}};
			double[,] tmp6 = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
			Matrix matrix1 = new Matrix(4, 5, tmp3);
			Matrix matrix2 = new Matrix(4, 5, tmp4);
			Matrix matrix3 = new Matrix(5, 5, tmp5);
			Matrix matrix4 = new Matrix(4, 5, tmp3);
			Matrix matrix5 = new Matrix(3, 3, tmp6);
			Console.WriteLine("\n******************************FOURTH TASK****************************\n");
			Console.WriteLine("First matrix:\n{0}", matrix1.ToString());
			Console.WriteLine("Second matrix:\n{0}", matrix2.ToString());
			Console.WriteLine("Third matrix:\n{0}", matrix3.ToString());
			Console.WriteLine("Fourth matrix:\n{0}", matrix4.ToString());
			Console.WriteLine("Fifth matrix:\n{0}", matrix5.ToString());
			Console.WriteLine("First + second:\n{0}", (matrix1 + matrix2).ToString());
			Console.WriteLine("First - second:\n{0}", (matrix1 - matrix2).ToString());
			Console.WriteLine("First * third:\n{0}", (matrix1 * matrix3).ToString());
			Console.WriteLine("First * 2:\n{0}", (matrix1 * 2).ToString());
			Console.WriteLine("First transposed matrix:\n{0}", matrix1.Transpose().ToString());
			Console.WriteLine("Submatrix(3x4) from (1; 1) in first:\n{0}", matrix1.SubMatrix(1, 1, 3, 4).ToString());
			
			if (matrix1 == matrix4)
			{
				Console.WriteLine("First matrix and fourth matrix are equals");
			}
			if (matrix1 != matrix2)
			{
				Console.WriteLine("First matrix and second matrix are not equals");
			}
			if(matrix3.IsSquareMatrix())
			{
				Console.WriteLine("Third is square matrix");
			}
			if(matrix5.IsSymetricMatrix())
			{
				Console.WriteLine("Fifth is symetric matrix");
			}
			if(matrix5.IsZeroMatrix())
			{
				Console.WriteLine("Fifth is zero matrix");
			}
			
			Console.ReadKey(); 
		}
	}
}